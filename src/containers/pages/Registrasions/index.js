import Login from './Login'
import Register from './Register'
import ForgotPw from './forgotpassword'
import SplashScreen from './SplashScreen'
import NavTobTab from './NavTobTab'

export {
    Login,
    Register,
    ForgotPw,
    SplashScreen,
    NavTobTab
}