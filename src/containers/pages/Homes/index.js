import Home from './Home'
import NewsDetail from './NewsDetail'
import Message from './Message'
import Keranjang from './Keranjang'
import Chat from './chat'
import Kontak from './Kontak'

export {
    Home,
    NewsDetail,
    Message,
    Keranjang,
    Chat,
    Kontak
}

